import React from 'react';
import {useState,useEffect} from 'react';
import { CardTitle,Card,CardText,CardActions,Button,CardMenu,IconButton } from 'react-mdl';
function Services() {
    useEffect(()=>{
        fetchItems()

    },[])
    const[items,setItems]=useState([]);
    const text="hello";
    const fetchItems=async()=>{
        const data=await fetch('https://bakesaleforgood.com/api/deals')

        const items=await data.json();
        console.log(items);
        setItems(items);
    }
       return(
     
        
      <div id="service" style={{display:"flex",flexWrap:'wrap',justifyContent:'space-around',position:"sticky",marginTop:"4%"}}>
        
      {items.map(item=>(
     <Card shadow={0} style={{width: '512px', margin: 'auto'}}>
     <CardTitle style={{color: '#fff', height: '176px', background: 'url(https://i.gadgets360cdn.com/large/mi_gaming_laptop_2019_image_1565003115644.jpg?output-quality=80&output-format=webp) center / cover'}}>{item.name}</CardTitle>
     <CardText>
         {item.title} <br></br>
        <b>Price:-</b> {item.price}
     
         
     </CardText>
     <CardActions border>
         <Button colored>Get Started</Button>
     </CardActions>
     <CardMenu style={{color: '#fff'}}>
         <IconButton name="share" />
     </CardMenu>
 </Card>
      ))}


      </div>
    )
  }

    export default Services;